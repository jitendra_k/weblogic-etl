package com.appnomic.appsone.weblogic.etl.source

import com.appnomic.appsone.weblogic.etl.cache.Cache
import com.appnomic.appsone.weblogic.etl.traits.HTTPScheduledSource
import com.appnomic.appsone.weblogic.etl.util.Constants
import groovy.util.logging.Slf4j
import groovyx.gpars.GParsPool
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response

import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSession
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit

/**
 * @author Jitendra Kumar : 23/11/18
 */
@Slf4j
class KPICollectionSource implements HTTPScheduledSource {
    MediaType json
    OkHttpClient httpClient

    @Override
    void init(Map<String, Object> config) {
        HTTPScheduledSource.super.init(config)
        Integer timeout = Integer.valueOf(config[Constants.TIMEOUT].toString())
        json = MediaType.parse("Content-Type:application/json;")

        httpClient = new OkHttpClient.Builder()
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .readTimeout(timeout, TimeUnit.SECONDS)
                .hostnameVerifier(new HostnameVerifier() {
            @Override
            boolean verify(String s, SSLSession sslSession) {
                return true
            }
        })
                .build()
        log.debug "Connect timeout : ${httpClient.connectTimeoutMillis()} , Read timeout : ${httpClient.readTimeoutMillis()}, Write timeout : ${httpClient.writeTimeoutMillis()}"

        this
    }

    /**
     * This method will run with a configured frequency and it helps to collect KPI data
     */
    @Override
    void fetchData() {
        ConcurrentHashMap serverDetails = Cache.getServerDetails()
        GParsPool.withPool (serverDetails.size()){
            serverDetails.eachParallel {
                url, serverDetail ->
                    RequestBody requestBody = RequestBody.create(json, "{}")
                    Request request = new Request.Builder().url(url)
                            //.post(requestBody)
                            .get()
                            .build()
                    log.debug "Requesting KPI with following url : $url"
                    Response response = httpClient.newCall(request).execute()
                    String body = response.body().string()
                    int statusCode = response.code()
                    log.debug "Status code : $statusCode"
                    log.debug "Received KPI data : $body"
                    close(response)
                    sendMessage(body)
            }
        }
    }
}
