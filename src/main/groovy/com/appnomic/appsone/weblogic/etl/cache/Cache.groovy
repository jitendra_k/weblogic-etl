package com.appnomic.appsone.weblogic.etl.cache

import com.appnomic.appsone.weblogic.etl.pojo.ServerDetails

import java.util.concurrent.ConcurrentHashMap

/**
 * @author Jitendra Kumar : 23/11/18
 */
class Cache {

    static ConcurrentHashMap serverDetailsMap = new ConcurrentHashMap()

    static void put(String url, ServerDetails serverDetails) {
        serverDetailsMap.put(url, serverDetails)
    }

    static ConcurrentHashMap getServerDetails() {
        return serverDetailsMap
    }
}
