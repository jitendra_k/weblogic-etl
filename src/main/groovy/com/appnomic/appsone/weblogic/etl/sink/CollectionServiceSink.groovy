package com.appnomic.appsone.weblogic.etl.sink

import com.appnomic.appsone.weblogic.etl.util.Constants
import com.appnomic.etl.lib.traits.Sink
import groovy.util.logging.Slf4j
import groovyx.gpars.actor.DefaultActor
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response

import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSession
import java.util.concurrent.TimeUnit

/**
 * @author Jitendra Kumar : 22/11/18
 */
@Slf4j
class CollectionServiceSink extends DefaultActor implements Sink {
    String connectionString
    MediaType text
    OkHttpClient httpClient

    @Override
    Sink init(Map<String, Object> map) {
        Integer timeout = Integer.valueOf(map[Constants.TIMEOUT].toString())
        connectionString = map[Constants.CONNECTION_URL]
        text = MediaType.parse("Content-Type:${map[Constants.CONTENT_TYPE]};")

        httpClient = new OkHttpClient.Builder()
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .readTimeout(timeout, TimeUnit.SECONDS)
                .hostnameVerifier(new HostnameVerifier() {
            @Override
            boolean verify(String s, SSLSession sslSession) {
                return true
            }
        })
                .build()
        log.debug "Connect timeout : ${httpClient.connectTimeoutMillis()} , Read timeout : ${httpClient.readTimeoutMillis()}, Write timeout : ${httpClient.writeTimeoutMillis()}"

        this
    }

    /**
     * Post KPI data to collection service
     */
    @Override
    void processMessage(Object obj) {
        if (obj == null) {
            log.warn(" Null object is received in sink.")
            return
        }
        def content = obj as String
        Response response
        try {
            RequestBody requestBody = RequestBody.create(text, content)
            Request request = new Request.Builder().url(connectionString)
                    .post(requestBody)
                    .build()
            log.debug("Posting data to server with following url : $connectionString")
            response = httpClient.newCall(request).execute()
            log.info("Posted data on server, recieved response code: ${response.code()} and body: ${response.body().string()}")

        } catch (Exception e) {
            log.error "Error in posting the data to url : $connectionString", e
        } finally {
            if (response) {
                response.close()
            }
        }
    }
}
