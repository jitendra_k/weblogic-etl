package com.appnomic.appsone.weblogic.etl.sink

import com.appnomic.appsone.weblogic.etl.util.Constants
import com.appnomic.etl.lib.traits.Sink
import groovy.util.logging.Slf4j
import groovyx.gpars.actor.DefaultActor
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response

import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSession
import java.util.concurrent.TimeUnit

/**
 * @author Jitendra Kumar : 23/11/18
 */
@Slf4j
class APIManagementSink extends DefaultActor implements Sink{
    String connectionString
    String apiMUrl
    MediaType json
    OkHttpClient httpClient

    @Override
    Sink init(Map<String, Object> map) {
        Integer timeout = Integer.valueOf(map[Constants.TIMEOUT].toString())
        connectionString = map[Constants.CONNECTION_URL]
        apiMUrl = map[Constants.API_MANAGEMENT_URL_CONSTANT]
        json = MediaType.parse("Content-Type:application/json;")

        httpClient = new OkHttpClient.Builder()
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .readTimeout(timeout, TimeUnit.SECONDS)
                .hostnameVerifier(new HostnameVerifier() {
            @Override
            boolean verify(String s, SSLSession sslSession) {
                return true
            }
        })
                .build()
        log.debug "Connect timeout : ${httpClient.connectTimeoutMillis()} , Read timeout : ${httpClient.readTimeoutMillis()}, Write timeout : ${httpClient.writeTimeoutMillis()}"

        this
    }

    @Override
    void processMessage(Object obj) {
        if (obj == null) {
            log.warn(" Null object is received in APIManagement sink.")
            return
        }
        def dataMap = obj as Map
        def enterpriseName = dataMap[Constants.ENTERPRISE_NAME] as String
        def instanceName = dataMap[Constants.INSTANCE_NAME] as String
        def checkInstanceUrl = "$apiMUrl$enterpriseName/customInstance/$instanceName"
        Response response
        try {
            Request request = new Request.Builder().url(checkInstanceUrl)
                    .get()
                    .build()
            log.debug("Checking custom instance with following url : $checkInstanceUrl")
            response = httpClient.newCall(request).execute()
            int resCode = response.code()
            /**
             * check if given instance is already available. If same instance is not found then send instance
             * creation request to API management
             */
            if (resCode == 302) {
                log.info "custom instance '$instanceName' is already available, no need to create."
            } else if (resCode == 204) {
                log.info "custom instance '$instanceName' is not available, so we are sending create request to API Management."
                def createInstanceUrl = "$apiMUrl$enterpriseName/customInstance"
                RequestBody requestBody = RequestBody.create(json, "{}")
                request = new Request.Builder().url(createInstanceUrl)
                        .post(requestBody)
                        .build()
                log.debug "Sending custome instace creation request with following url : $createInstanceUrl"
                response = httpClient.newCall(request).execute()
                log.debug "Posted data to API Management, recieved response code: ${response.code()} and body: ${response.body().string()}"
            }

        } catch (Exception e) {
            log.error "Error in posting the data to url : $checkInstanceUrl", e
        } finally {
            if (response) {
                response.close()
            }
        }
    }
}
