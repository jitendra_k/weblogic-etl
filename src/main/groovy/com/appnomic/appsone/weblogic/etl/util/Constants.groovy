package com.appnomic.appsone.weblogic.etl.util

/**
 * @author Jitendra Kumar : 22/11/18
 */
class Constants {
    private Constants() {}
    public static final String CONNECTION_URL = "ConnectionUrl"
    public static final String TIMEOUT = "Timeout"
    public static final String CONTENT_TYPE = "ContentType"
    public static final String INSTANCE_NAME = "instanceName"
    public static final String ENTERPRISE_NAME = "instanceName"

    public static final String API_MANAGEMENT_URL = "wl.etl.api.management.url"
    public static final String API_MANAGEMENT_URL_DEFAULT = "http://192.168.2.237:8080/opdashboard/rest/definition/"
    public static final String API_MANAGEMENT_URL_CONSTANT = "APIMURL"
    public static final String HTTP_CONTENT_TYPE_DEFAULT = "text/xml"

    public static final String HTTPSERVER_TIMEOUT = "http.timeout"
    public static final String HTTPSERVER_TIMEOUT_DEFAULT_VALUE = "50"

    public static final String KPI_COLLECTION_FREQ_PROP = "wl.etl.kpi.collection.frequency"
    public static final String KPI_COLLECTION_FREQ_DEF = "1"
    public static final String KPI_COLLECTION_FREQ = "KPICollectionFreq"

    public static final String KEYSTORE_FILE_NAME = "keystore.file.name"
    public static final String KEYSTORE_FILE_NAME_DEFUALT = "server.jks"
    public static final String KEYSTORE_PASSWORD = "keystore.password"
    public static final String KEYSTORE_PASSWORD_DEFUALT = "z0bOXVvilgXXOI+V7FwnAA=="

    public static final String APIS_TO_ETL_NOTIFICATION_PORT_PROP = "apis.to.wl.etl.notification.port"
    public static final String APIS_TO_ETL_NOTIFICATION_PORT_DEF = "8870"
    public static final String APIS_TO_ETL_NOTIFICATION_PORT = "APIsToETLPort"
    public static final String APIS_TO_ETL_NOTIFICATION_IP_PROP = "apis.to.wl.etl.notification.ip"
    public static final String APIS_TO_ETL_NOTIFICATION_IP_DEF = "localhost"
    public static final String APIS_TO_ETL_NOTIFICATION_IP = "APIsToETLIP"
    public static final String APIS_TO_ETL_NOTIFICATION_PATH = "/NotifyWLETL"
}
