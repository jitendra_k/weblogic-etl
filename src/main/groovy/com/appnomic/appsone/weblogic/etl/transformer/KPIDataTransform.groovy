package com.appnomic.appsone.weblogic.etl.transformer

import com.appnomic.appsone.weblogic.etl.sink.APIManagementSink
import com.appnomic.etl.lib.traits.Sink
import com.appnomic.etl.lib.traits.Transform
import groovy.util.logging.Slf4j
import groovyx.gpars.actor.DefaultActor

/**
 * @author Jitendra Kumar : 22/11/18
 */
@Slf4j
class KPIDataTransform extends DefaultActor implements Transform{
    APIManagementSink apiManagementSink

    @Override
    Object transformMessage(Object o) {
        log.info o.toString()
        //TODO:if KPI data are found for a new instance then call API management sink to check and create new instance
        //TODO:otherwise filter KPI data and send to collection sink
        /*if (o instanceof String) {
            apiManagementSink << o
        }*/
        this.getSink() << o
        return null
    }

    @Override
    Sink init(Map<String, Object> map) {
        return null
    }

    void setApiManagementSink(Sink apiManagementSink) {
        this.apiManagementSink = apiManagementSink
    }
}
