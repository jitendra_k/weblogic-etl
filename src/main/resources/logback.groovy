import ch.qos.logback.classic.encoder.PatternLayoutEncoder

def LOG_PATH = "../logs"
appender("LOGGER-FILE", RollingFileAppender) {
    file = "${LOG_PATH}/weblogic-etl.log"
    encoder(PatternLayoutEncoder) {
        pattern = "%d{yyyy-MM-dd HH:mm:ss} - %-5level %logger{36} - %msg%n"
    }
    rollingPolicy(SizeAndTimeBasedRollingPolicy) {
        fileNamePattern = "${LOG_PATH}/archived/weblogic-etl.%d{yyyy-MM-dd}.%i.log"
        maxFileSize = "5MB"
        maxHistory = 7
        totalSizeCap = "100MB"
    }
}

logger("com.appnomic.appsone.weblogic.etl", INFO, ["LOGGER-FILE"], false)
root(INFO, ["LOGGER-FILE"])