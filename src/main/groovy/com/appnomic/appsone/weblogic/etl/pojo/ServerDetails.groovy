package com.appnomic.appsone.weblogic.etl.pojo

/**
 * @author Jitendra Kumar : 23/11/18
 */
class ServerDetails {
    String name
    String url

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    String getUrl() {
        return url
    }

    void setUrl(String url) {
        this.url = url
    }

    @Override
    public String toString() {
        return "ServerDetails{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
