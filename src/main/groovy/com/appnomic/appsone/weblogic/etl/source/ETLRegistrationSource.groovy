package com.appnomic.appsone.weblogic.etl.source

import com.appnomic.appsone.common.encryption.PasswordConvertor
import com.appnomic.appsone.weblogic.etl.cache.Cache
import com.appnomic.appsone.weblogic.etl.pojo.ServerDetails
import com.appnomic.appsone.weblogic.etl.util.Constants
import com.appnomic.etl.lib.traits.RestEventSource
import com.appnomic.etl.lib.util.ConfProperties
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import spark.Service

import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSession
import java.util.concurrent.TimeUnit

/**
 * @author Jitendra Kumar : 22/11/18
 */
@Slf4j
class ETLRegistrationSource implements RestEventSource{
    MediaType json
    OkHttpClient httpClient
    String apiRegistrationUrl = "http://www.mocky.io/v2/5bfbc56d310000290039ba40"

    def keystoreFileName = ConfProperties.getString(Constants.KEYSTORE_FILE_NAME, Constants.KEYSTORE_FILE_NAME_DEFUALT)
    def keystorePassword = ConfProperties.getString(Constants.KEYSTORE_PASSWORD, Constants.KEYSTORE_PASSWORD_DEFUALT)
    static Service https

    def passwordConverter = new PasswordConvertor()

    @Override
    void exposeRoutes() {
        //Request from APIs
        https.post "$Constants.APIS_TO_ETL_NOTIFICATION_PATH", {request,response ->
            def body = request.body().toString()
            log.debug "Data received as a notification from APIs :- '$body'"
            sendMessage(body)
            "Received Ok"

        }
    }

    @Override
    void init(Map<String, Object> config) {
        Integer timeout = Integer.valueOf(config[Constants.TIMEOUT].toString())
        json = MediaType.parse("Content-Type:application/json;")

        httpClient = new OkHttpClient.Builder()
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .readTimeout(timeout, TimeUnit.SECONDS)
                .hostnameVerifier(new HostnameVerifier() {
            @Override
            boolean verify(String s, SSLSession sslSession) {
                return true
            }
        })
                .build()
        log.debug "Connect timeout : ${httpClient.connectTimeoutMillis()} , Read timeout : ${httpClient.readTimeoutMillis()}, Write timeout : ${httpClient.writeTimeoutMillis()}"

        //Send registration request to APIs and get initial configuration from APIs
        initialiseCache()

        def ip = config[Constants.APIS_TO_ETL_NOTIFICATION_IP] as String
        def listenPort = config[Constants.APIS_TO_ETL_NOTIFICATION_PORT] as Integer
        String keystoreFilePath = ETLRegistrationSource.class.getClassLoader().getResource(keystoreFileName).getFile();
        String decryptedPwd =  passwordConverter.getDecryptedPassword(keystorePassword)

        https = Service.ignite()
        https.port(listenPort)
        https.secure(keystoreFilePath, decryptedPwd, null, null);
        log.info("WebLogic ETL server started on https protocol with ip:$ip, soap port:$listenPort.")
        this
    }

    private void initialiseCache() {
        Request request = new Request.Builder().url(apiRegistrationUrl)
                .get()
                .build()
        log.info("Sending WebLogic ETL registration request with following url : $apiRegistrationUrl")
        Response response = httpClient.newCall(request).execute()
        String body = response.body().string()
        log.debug "WebLogic ETL received configuration from APIs : $body"
        //Load config data into cache
        List<ServerDetails> serverDetails = new JsonSlurper().parseText(body) as List<ServerDetails>
        for (ServerDetails s : serverDetails) {
            Cache.put(s.getUrl(), s)
        }
        //Close all resources
        response?.body()?.close()
    }
}
