package com.appnomic.appsone.weblogic.etl.transformer

import com.appnomic.appsone.weblogic.etl.cache.Cache
import com.appnomic.appsone.weblogic.etl.pojo.ServerDetails
import com.appnomic.etl.lib.traits.Sink
import com.appnomic.etl.lib.traits.Transform
import groovy.json.JsonSlurper
import groovyx.gpars.actor.DefaultActor

/**
 * @author Jitendra Kumar : 23/11/18
 */
class ETLRegistrationTransform extends DefaultActor implements Transform{
    @Override
    Object transformMessage(Object o) {
        if (o instanceof String) {
            //Update cache when APIs notify to WL ETL
            //TODO:If a new instance is found through notification then, call APIManagement Sink to check and create same instance
            List<ServerDetails> serverDetails = new JsonSlurper().parseText(o) as List<ServerDetails>
            for (ServerDetails s : serverDetails) {
                Cache.put(s.getUrl(), s)
            }
        }
        return null
    }

    @Override
    Sink init(Map<String, Object> map) {
        return null
    }
}
