package com.appnomic.appsone.weblogic.etl.traits

import com.appnomic.etl.lib.traits.Source

/**
 * @author Jitendra Kumar : 23/11/18
 */
trait ScheduledSource implements Source {

    abstract void fetchData()

    void schedule(Closure operation, int delay, int interval) {
        new Timer().scheduleAtFixedRate({
            operation()
        } as TimerTask, delay * 1000, interval * 1000)
    }
}
