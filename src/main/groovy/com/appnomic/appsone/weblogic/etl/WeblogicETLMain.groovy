package com.appnomic.appsone.weblogic.etl

import com.appnomic.appsone.weblogic.etl.sink.APIManagementSink
import com.appnomic.appsone.weblogic.etl.sink.CollectionServiceSink
import com.appnomic.appsone.weblogic.etl.source.ETLRegistrationSource
import com.appnomic.appsone.weblogic.etl.source.KPICollectionSource
import com.appnomic.appsone.weblogic.etl.transformer.ETLRegistrationTransform
import com.appnomic.appsone.weblogic.etl.transformer.KPIDataTransform
import com.appnomic.appsone.weblogic.etl.util.Constants
import com.appnomic.etl.lib.util.ConfProperties
import com.google.common.util.concurrent.ServiceManager
import groovy.util.logging.Slf4j

/**
 * @author Jitendra Kumar : 22/11/18
 */

@Slf4j
class WeblogicETLMain {
    static void main(String... args){
        log.info "Starting WebLogic ETL"

        def config = new HashMap<String,Object>()

        def apiMUrl = ConfProperties.getString(Constants.API_MANAGEMENT_URL, Constants.API_MANAGEMENT_URL_DEFAULT)
        def timeout = ConfProperties.getString(Constants.HTTPSERVER_TIMEOUT, Constants.HTTPSERVER_TIMEOUT_DEFAULT_VALUE)
        def kpiCollFreq = ConfProperties.getString(Constants.KPI_COLLECTION_FREQ_PROP, Constants.KPI_COLLECTION_FREQ_DEF) as Integer
        def apisToEtlPort = ConfProperties.getString(Constants.APIS_TO_ETL_NOTIFICATION_PORT_PROP, Constants.APIS_TO_ETL_NOTIFICATION_PORT_DEF)
        def apisToEtlIP = ConfProperties.getString(Constants.APIS_TO_ETL_NOTIFICATION_IP_PROP, Constants.APIS_TO_ETL_NOTIFICATION_IP_DEF)
        config[Constants.API_MANAGEMENT_URL_CONSTANT] = apiMUrl
        config[Constants.TIMEOUT] = timeout
        config[Constants.KPI_COLLECTION_FREQ] = kpiCollFreq*60
        config[Constants.APIS_TO_ETL_NOTIFICATION_PORT] = apisToEtlPort
        config[Constants.APIS_TO_ETL_NOTIFICATION_IP] = apisToEtlIP

        APIManagementSink apiManagementSink = new APIManagementSink()
        apiManagementSink.init(config)
        apiManagementSink.start()

        ETLRegistrationTransform apiRegistrationTransform = new ETLRegistrationTransform()
        apiRegistrationTransform.init(config)
        apiRegistrationTransform.start()
        apiRegistrationTransform.setSink(apiManagementSink)

        ETLRegistrationSource etlRegistrationSource = new ETLRegistrationSource()
        etlRegistrationSource.init(config)
        etlRegistrationSource.start()
        etlRegistrationSource.setSink(apiRegistrationTransform)

        CollectionServiceSink collectionServiceSink = new CollectionServiceSink()
        collectionServiceSink.init(config)
        collectionServiceSink.start()

        KPIDataTransform kPIDataTransform = new KPIDataTransform()
        kPIDataTransform.init(config)
        kPIDataTransform.start()
        kPIDataTransform.setSink(collectionServiceSink)
        kPIDataTransform.setApiManagementSink(apiManagementSink)

        KPICollectionSource kpiCollectionSource = new KPICollectionSource()
        kpiCollectionSource.init(config)
        kpiCollectionSource.setSink(kPIDataTransform)

    }

    public static void addShutdownHook(final ServiceManager serviceManager)
    {
        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                log.info("Shutdown signal received");
                log.info("Stopping WebLogic ETL.");
                serviceManager.stopAsync();
            }
        });
    }
}
