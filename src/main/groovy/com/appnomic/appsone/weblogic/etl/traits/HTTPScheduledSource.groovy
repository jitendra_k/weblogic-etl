package com.appnomic.appsone.weblogic.etl.traits

import com.appnomic.appsone.weblogic.etl.util.Constants
import groovy.util.logging.Slf4j
import okhttp3.OkHttpClient
import okhttp3.Response

/**
 * @author jitendra on 23/11/18.
 */
@Slf4j
trait HTTPScheduledSource implements ScheduledSource {

    int fetchInterval
//    OkHttpClient httpClient

    @Override
    void init(Map<String, Object> config) {

        // Read the fetch interval and database details from configuration
        fetchInterval = config[Constants.KPI_COLLECTION_FREQ]

        // Schedule fetching AppsOne Alerts, Transactions or Kpi's. Pass implemented 'fetchData' to the scheduler
        log.info "Scheduling source for every $fetchInterval Seconds"
        schedule(this.&fetchData, 0, fetchInterval)

    }

    /**
     * This method has to be called by Custom ScheduledSource sub-classes to clean up the datasource.
     */
    void close(Response response){
        //Close all resources
        response?.body()?.close()
    }
}